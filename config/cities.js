module.exports = {
  'Chile/Continental': 'Chile (Continental)',
  'Australia/NSW': 'New South Wales',
  'Asia/Katmandu': 'Kathmandu',
  'Antarctica/DumontDUrville': 'Dumont d\'Urville',
  'HST': 'Hawaii-Aleutian Time Zone',
  'NZ-CHAT': 'New Zealand',
};
