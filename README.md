# Timezone grouping

## HOW TO

### Export grouped timestamps

1. (OPTIONAL) Adapt `config/database.js` based on the data that you have. (*)
2. (OPTIONAL) Adapt `config/cities.js` in case you want translations for your city names.
3. Run `npm run start`
4. Result will be printed into a file `result.js`

*) The script allows numbers for each TZ but you can also leave it empty. They are meant for cases where you want to let them sort by, e.g., number of people that live in the TZ.

### Export database script

1. Run `node export.js <table_name> <column_name>`
2. Result will be printed into a file `result_.sql`
