const timezones = require('./result');
const { saveFile, mapTimezoneToUpdateScript } = require('./utils');

let result = '';
const [tableName, columnName] = process.argv.slice(2);

timezones.forEach(timezone => {
  result += mapTimezoneToUpdateScript(timezone, tableName, columnName);
});

// write results into a .js file
const filename = `result.sql`;
saveFile(filename, result);